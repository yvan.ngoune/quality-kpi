"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")

def kpi_mass(system: LegoAssembly)->float:
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_mass (float): Sum of masses of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass()")

    total_mass = 0
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
    return total_mass # alternative: sum(c.properties["mass [g]"] for c in system.get_component_list(-1))

def kpi_cost(system: LegoAssembly)->float:
    """
    Calculates the total costs of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_costs (float): Sum of costs of all components in the system in €

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_cost()")

    total_cost = 0
    for c in system.get_component_list(-1):
        total_cost += c.properties["price [Euro]"]
    return total_cost # alternative: sum(c.properties["price [g]"] for c in system.get_component_list(-1))

def kpi_delivery_time(system: LegoAssembly)->float:
    """
    Calculates the longest delivery time

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        latest_delivery_times (float): The longest delivery time in days

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """   
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_delivery_time()")
        
    delivery_times = []
    for c in system.get_component_list(-1):
        delivery_times.append(c.properties["delivery time [days]"])
        
    latest_delivery_times = max(delivery_times)
    return latest_delivery_times

def kpi_number_of_components(system: LegoAssembly)->float:
    """
    Calculates number of components used

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        length (float): number of components used

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_delivery_time()")
        
    list = system.get_component_list(-1)
    length = len(list)
    return length

# Add new functions for calculating metrics


if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
